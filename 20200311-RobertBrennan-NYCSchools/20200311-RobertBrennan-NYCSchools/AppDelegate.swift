//
//  AppDelegate.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootNavigationController:UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //MARK: - Setup Root View Controller
        window = UIWindow(frame: UIScreen.main.bounds)
        var vc = HighSchoolListViewController()
        vc = HighSchoolListViewController(nibName: "HighSchoolListViewController", bundle: nil)
        self.rootNavigationController = UINavigationController(rootViewController: vc)
        self.rootNavigationController!.setNavigationBarHidden(false, animated: false)
        self.window?.rootViewController = self.rootNavigationController
        self.window?.makeKeyAndVisible()
        return true
    }
}

