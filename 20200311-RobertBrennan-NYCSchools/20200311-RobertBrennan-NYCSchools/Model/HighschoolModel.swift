//
//  HighschoolModel.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import Foundation

//MARK: School Model Data
struct SchoolData {
    var schoolID: String?
    var schoolLocation: String?
    var schoolName: String?
    var testTakers: String?
    var mathSAT: String?
    var readingSAT: String?
    var writingSAT: String?

    init(schoolID: String? = nil,
         schoolName: String? = nil,
         schoolLocation: String? = nil,
         testTakers: String? = nil,
         mathSAT: String? = nil,
         readingSAT: String? = nil,
         writingSAT: String? = nil) {

        self.schoolID = schoolID
        self.schoolLocation = schoolLocation
        self.schoolName = schoolName
        self.testTakers = testTakers
        self.mathSAT = mathSAT
        self.readingSAT = readingSAT
        self.writingSAT = writingSAT
    }
}
