//
//  NavigationBarExtension.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Highschool List NavBar
extension HighSchoolListViewController {

    func setupNavigationBarItems() {
        setupRemainingNavItems()
        setupLeftNavItem()
        setupRightNavItem()
    }
    private func setupRemainingNavItems() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        let extraview = UIView()
        extraview.backgroundColor = .white
        extraview.layer.zPosition = -1
        extraview.frame = CGRect(x: 0, y: (navigationController?.navigationBar.frame.height)!, width: view.frame.width, height: 20)
        navigationController?.navigationBar.addSubview(extraview)
    }
    private func setupLeftNavItem() {
        let viewContainer = UIView()
        let titleLabel = UILabel()
        titleLabel.text = "NYC Schools"
        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        titleLabel.frame = CGRect(x: 5, y: -10, width: 200, height: 80)
        titleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        viewContainer.frame =  CGRect(x: 0, y: 40, width: 200, height: 80)
        viewContainer.layer.zPosition = 1
        viewContainer.addSubview(titleLabel)
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: viewContainer)]
    }
    private func setupRightNavItem() {
        let viewContainer = UIView()
        let redView = UIView()
        let titleLabel = UILabel()
        redView.clipsToBounds = true
        redView.layer.cornerRadius = 0.5 * redView.frame.width
        redView.backgroundColor = .systemRed
        redView.frame = CGRect(x: 0, y: 20, width: 10, height: 10)

        titleLabel.text = " No SAT Results"
        titleLabel.textColor = .black
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 12)
        titleLabel.frame = CGRect(x: 12, y: 20, width: 90, height: 30)
        titleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        viewContainer.frame =  CGRect(x: 0, y: 40, width: 100, height: 80)
        redView.center.y = titleLabel.center.y

        viewContainer.layer.zPosition = 1
        viewContainer.clipsToBounds = true
        viewContainer.addSubview(titleLabel)
        viewContainer.addSubview(redView)
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: viewContainer)]

    }
}

// MARK: - Highschool Details NavBar
extension HighschoolDetailsViewController {

    func setupNavigationBarItems() {
        setupRemainingNavItems()
        setupLeftNavItem()
    }
    private func setupRemainingNavItems() {
        let schoolName = schooldata[0].schoolName
        let schoolLabel = UILabel()
        schoolLabel.text = schoolName
        schoolLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        schoolLabel.numberOfLines = 2
        schoolLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        schoolLabel.textColor = .black
        self.navigationItem.titleView?.addSubview(schoolLabel)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    private func setupLeftNavItem() {
        let backArrowButton = UIButton(type: .system)
        backArrowButton.setImage(#imageLiteral(resourceName: "back_arrow").withRenderingMode(.alwaysOriginal), for: .normal)
        backArrowButton.frame = CGRect(x: 0, y: 0, width: 50, height: 80)
        backArrowButton.addTarget(self, action: #selector(dismissView)
            , for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backArrowButton)]
    }
}

