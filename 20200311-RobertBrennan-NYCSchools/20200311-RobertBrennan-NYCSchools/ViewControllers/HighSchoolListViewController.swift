//
//  HighSchoolListViewController.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import UIKit
class HighSchoolListViewController: UIViewController {

    // MARK: - Variables
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.separatorStyle = .none
        view.showsVerticalScrollIndicator = false
        view.clipsToBounds = true

        return view
    }()

    var schoolData = [SchoolData]()
    var uniqueArray = [SchoolData]()
    let viewContainer = UIView()

    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchHighSchools()
        configureTableView()
        setupNavigationBarItems()
    }
}

// MARK: Functions
extension HighSchoolListViewController{
    // TableView Setup
    fileprivate func configureTableView() {
        view.addSubview(tableView)
        tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        tableView.backgroundColor = .white
        tableView.indicatorStyle = .white
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = true
        tableView.register(UINib(nibName: "HighSchoolListTableViewCell", bundle: nil), forCellReuseIdentifier: "HighSchoolCell")
    }
    // Fetch School Data
    func fetchHighSchools() {
        ApiService.sharedInstance.fetchSchoolData { (SchoolData) in
            self.schoolData = SchoolData
            self.tableView.reloadData()
        }
    }
}

// MARK: Table View Delegates
extension HighSchoolListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 164
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HighSchoolCell", for: indexPath) as! HighSchoolListTableViewCell
        let data = schoolData[indexPath.row]
        cell.resultsIndicator.isHidden = true
        cell.selectionStyle = .none
        cell.highschoolLabel.text = data.schoolName

        if data.mathSAT == nil{
            cell.resultsIndicator.isHidden = false
        }
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = schoolData[indexPath.row]
        let vc = HighschoolDetailsViewController()
        vc.schooldata = [data]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
