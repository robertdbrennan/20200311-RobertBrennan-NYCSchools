//
//  HighschoolDetailsViewController.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import UIKit

class HighschoolDetailsViewController: UIViewController {
    
    // MARK: - Variables
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!

    var highschoolNavigationTitle: UILabel?
    var schooldata = [SchoolData]()

    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItems()
        setupLabels()
    }
}

// MARK: Functions
extension HighschoolDetailsViewController{
    // View UI
    func setupLabels(){
        let schoolText     = schooldata[0].schoolName ?? ""
        let mathText       = "🧮 Math Score: \(schooldata[0].mathSAT ?? "Results Unavailable")"
        let readingText    = "📚 Reading Score: \(schooldata[0].readingSAT ?? "Results Unavailable")"
        let writingText    = "📝 Writing Score: \(schooldata[0].writingSAT ?? "Results Unavailable")"
        let testTakersText = "# of Test Takers: \(schooldata[0].testTakers ?? "Results Unavailable")"

        schoolLabel.text     = schoolText
        mathLabel.text       = mathText
        readingLabel.text    = readingText
        writingLabel.text    = writingText
        testTakersLabel.text = testTakersText
    }
    @objc func dismissView(){
        self.navigationController?.popViewController(animated: true)
    }
}
