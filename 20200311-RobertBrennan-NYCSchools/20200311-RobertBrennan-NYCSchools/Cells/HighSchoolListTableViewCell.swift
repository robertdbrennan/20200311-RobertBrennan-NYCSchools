//
//  HighSchoolListTableViewCell.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import UIKit

class HighSchoolListTableViewCell: UITableViewCell {

    // MARK: - Variables
    @IBOutlet weak var resultsIndicator: UIView!
    @IBOutlet weak var highschoolContentView: UIView!
    @IBOutlet weak var highschoolLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

// MARK: - Functions
extension HighSchoolListTableViewCell{
    // Cell UI
    func setupView(){
        resultsIndicator.isHidden = true
        highschoolContentView.layer.cornerRadius = 15
        highschoolContentView.layer.shadowColor = UIColor.black.cgColor
        highschoolContentView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        highschoolContentView.layer.masksToBounds = false
        highschoolContentView.layer.shadowRadius = 3.0
        highschoolContentView.layer.shadowOpacity = 0.2

        resultsIndicator.clipsToBounds = true
        resultsIndicator.layer.cornerRadius = 0.5 * resultsIndicator.frame.width
    }
}
