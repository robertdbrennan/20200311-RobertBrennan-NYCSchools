//
//  ApiService.swift
//  20200311-RobertBrennan-NYCSchools
//
//  Created by Robert Brennan on 3/11/20.
//  Copyright © 2020 Creatility. All rights reserved.
//

import Foundation
import UIKit

class ApiService: NSObject {

    // MARK: - Variables
    static let sharedInstance = ApiService()
    let baseUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let detailsUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    var highSchools = [SchoolData]()

    func fetchSchoolData(completion: @escaping ([SchoolData]) -> ()) {
        self.fetchSchools(urlString: "\(self.baseUrl)", completion: completion)
    }

    // MARK: - Query School
    func fetchSchools(urlString: String, completion: @escaping ([SchoolData]) -> ()) {
        let dispatchGroup = DispatchGroup()
        let url = URL(string: urlString)
        dispatchGroup.enter()
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error as Any)
                return
            }
            do {
                //MARK: -  Parse data for School Name, Location and ID
                let results = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                for dictionary in results as! [[String: AnyObject]] {
                    guard let schoolName = dictionary["school_name"] as? String,
                        let schoolID = dictionary["dbn"] as? String,
                        let location = dictionary["location"] as? String else {
                            print("JSON data error")
                            break
                    }
                    self.highSchools.append(SchoolData(schoolID: schoolID, schoolName: schoolName, schoolLocation: location))
                }
                DispatchQueue.main.async {
                    //MARK: - Fetch School Details
                    self.fetchSchoolDetail(urlString: "\(self.detailsUrl)", completion: completion)
                    dispatchGroup.leave()
                }

            } catch let jsonError {
                print(jsonError)
            }
        }.resume()
    }

    // MARK: - Query School Details
    func fetchSchoolDetail(urlString: String, completion: @escaping ([SchoolData]) -> ()) {
        let dispatchGroup = DispatchGroup()
        let url = URL(string: urlString)
        dispatchGroup.enter()
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error as Any)
                return
            }
            do {
                let results = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                print(results)

                //MARK: -  Parse data for School ID and SAT Scores
                for dictionary in results as! [[String: AnyObject]] {
                    guard let schoolID = dictionary["dbn"] as? String,
                        let testTakers = dictionary["num_of_sat_test_takers"] as? String,
                        let readingSAT = dictionary["sat_critical_reading_avg_score"] as? String,
                        let mathSAT = dictionary["sat_math_avg_score"] as? String,
                        let writingSAT = dictionary["sat_writing_avg_score"] as? String else {
                            print("JSON data error")
                            break
                    }
                    //MARK: -  Search data by ID and insert new values
                    let indexOfSchool = self.highSchools.firstIndex(where: { $0.schoolID == schoolID})
                    if indexOfSchool != nil {
                        self.highSchools[indexOfSchool!].testTakers = testTakers
                        self.highSchools[indexOfSchool!].mathSAT = mathSAT
                        self.highSchools[indexOfSchool!].readingSAT = readingSAT
                        self.highSchools[indexOfSchool!].writingSAT = writingSAT
                    }
                    else{
                        print("School ID is not in the SAT data set.")
                    }
                }
                DispatchQueue.main.async {
                    //MARK: - Return Schools with Details
                    completion(self.highSchools)
                    dispatchGroup.leave()
                }
            } catch let jsonError {
                print(jsonError)
            }
        }.resume()
    }
}
